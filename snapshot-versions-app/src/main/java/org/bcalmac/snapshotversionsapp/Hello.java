package org.bcalmac.snapshotversionsapp;

import java.io.IOException;
import java.util.Properties;

public class Hello {

	public static void main(String[] args) throws IOException {
		System.out.println("The commit for lib is: " + getCommitForLib());
	}

	private static String getCommitForLib() throws IOException {
		Properties properties = new Properties();
		properties.load(ClassLoader.getSystemResourceAsStream("lib-version.properties"));
		return properties.getProperty("commit");
	}
}
